
float triggerWeight3D(float pt1,float pt2,float pt3, int flavor)
{
  if(flavor==111){//3 e event
    return (1-(1-doubleeg23(pt1)*doubleeg12(pt2)*doubleegdz)*(1-doubleeg23(pt2)*doubleeg12(pt3)*doubleegdz)*(1-doubleeg12(pt3)*doubleeg23(pt1)*doubleegdz));
  }
  if(flavor==112){//2 e + 1mu event

    return 1-(1-doubleeg23(pt1)*doubleeg12(pt2)*doubleegdz)*(1-(1-(1-muonegele23(pt1)*muonegmu8(pt3))*(1-muonegele12not23(pt1)*muonegmu23(pt3))))*(1-(1-(1-muonegele23(pt2)*muonegmu8(pt3))*(1-muonegele12not23(pt2)*muonegmu23(pt3))));
    
  }
  if(flavor==122){//1 e+ 2 mu event
    
    return 1-(1-doublemutk(pt2)*doublemutk(pt3)*doublemutkdz)*(1-(1-(1-muonegele23(pt1)*muonegmu8(pt2))*(1-muonegele12not23(pt1)*muonegmu23(pt2))))*(1-(1-(1-muonegele23(pt1)*muonegmu8(pt3))*(1-muonegele12not23(pt1)*muonegmu23(pt3))));
  }
  if(flavor==222){//3 mu event
    return (1-(1-doublemutk(pt1)*doublemutk(pt2)*doublemutkdz)*(1-doublemutk(pt2)*doublemutk(pt3)*doublemutkdz)*(1-doublemutk(pt3)*doublemutk(pt1)*doublemutkdz));
  }
  else return 0. ;
}

float triggerWeight2D(float pt1,float pt2, int flavor)
{
  if(flavor==11){//DoubleEG triggers
    return doubleeg23(pt1)*doubleeg12(pt2)*doubleegdz;    
  }
  if(flavor==22){//DoubleMu triggers
    return doublemutk(pt1)*doublemutk(pt2)*doublemutkdz;    
  }
  if(flavor==12){//MuEG triggers
    return (1-(1-muonegele23(pt1)*muonegmu8(pt2))*(1-muonegele12not23(pt1)*muonegmu23(pt2)));    
  }
  // if(flavor==21){//MuEG triggers
  //   return muonegele(pt2)*muonegmu(pt1);    
  // }
  if(flavor==2){//isotkmu22
    return 1-(1-isomutk22(pt1))*(1-isomutk22(pt2));
}
  else return 0. ;
  
}

float triggerWeightdimugl(float pt1,float pt2, int flavor=22)
{
  return doublemu(pt1)*doublemu(pt2)*doublemudz;    
}
