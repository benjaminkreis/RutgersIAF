#include <sys/stat.h>
#include <unistd.h>

#include "RutgersIAF/AnalysisPresenter/interface/Assembler.h"
#include "RutgersIAF/AnalysisPresenter/interface/Bundle.h"
#include "RutgersIAF/AnalysisPresenter/interface/Channel.h"
#include "RutgersIAF/AnalysisPresenter/interface/PhysicsContribution.h"

#include "helperAnalysisPresenter_hybrid_13TeV.C"

void ControlTT16() {
	///////////////////////
	// Binning/selection //
	///////////////////////
	
	// Specify axes and bins of multidimensional histogram
	// for ttbar 2L
	std::string varexp = "NLIGHTLEPTONS{2,5}:HT{0,1500,30}:MET{0,500,10}:ST{0,2000,20}:NGOODJETS{0,8}:NPROMPTNONISOINCLUSIVETRACKS7{0,25}:NGOODTRACKS{0,15}:NGOODINCLUSIVETRACKS{0,15}:LT{50,550,50}";
	varexp += ":NBJETSCSVL{0,4}:NGOODTAUS[0]{0,2}:MLEPTONS[0]{10,120,22}";
	varexp += ":NBJETSCSVM{0,4}:PTGOODELECTRONS[0]{10,210,40}:PTGOODMUONS[0]{10,210,40}";
	
	// Global cuts, if desired
	TString selection = "(NLIGHTLEPTONS == 2 && NGOODELECTRONS ==1 && NGOODMUONS ==1 && Sum$(QGOODELECTRONS) + Sum$(QGOODMUONS) == 0)";
	selection+= "&& PTGOODLEPTONS[0] > 25 && PTGOODLEPTONS[1] > 15";
	//selection+= "&& NGOODTAUS[0] == 0";
	////////////////////////
	// Initialize and run //
	////////////////////////
	Assembler* assembler = new Assembler();
	init(assembler,"MC");
	
	assembler->setDefaultBundle(assembler->getBundle("presentationBundle"));
	//assembler->setMode("noRatioPlot");
	assembler->setMode("noTTsystematics");
	assembler->setMode("fullPrecision");	
	setupData(assembler,true);
	setupBackgroundMC(assembler,true,true);
	setupBackgroundDD(assembler);
	setupFakeRates(assembler);
//	assembler->setFakeRate("nTrackFakeElectrons", "0");
//	assembler->setFakeRate("nTrackFakeMuons", "0");
//	assembler->setFakeRate("nPhotonFakeElectrons", "0");
//	assembler->setFakeRate("nPhotonFakeMuons", "0");
//	assembler->setFakeRate("nTauFakeTaus", "0");
	assembler->setDebug(true);
	prepare(assembler);
	assembler->process(varexp, selection);
	// At this point, we have the multidimensional histogram in memory and can start taking projections (tables, 1d histograms, ...)
	
	mkdir("ttbar", 0755);
	chdir("ttbar");
	
	assembler->setRange("NLIGHTLEPTONS", 2, 2);
	//assembler->setRange("NBJETSCSVM", 1);
	
	assembler->setRange("ST", 300);
	makeNicePlot(assembler->project("NGOODJETS", true)->plot(true),"n_{jets}")->SaveAs("ttbar_NGOODJETS_STgt300.png");
	makeNicePlot(assembler->project("MLEPTONS[0]", true)->plot(true),"M_{LL}","GeV")->SaveAs("ttbar_MLEPTONS_STgt300.png");
	assembler->project("NGOODJETS", true)->print();
	makeNicePlot(assembler->project("PTGOODELECTRONS[0]", true)->plot(true),"Leading Electron pT", "GeV")->SaveAs("ttbar_PTGOODELECTRONSLead_STgt300.png");
	makeNicePlot(assembler->project("PTGOODMUONS[0]", true)->plot(true),"Leading Muon pT", "GeV")->SaveAs("ttbar_PTGOODMUONSLead_STgt300.png");
	writeUncertainties(assembler->project("NGOODJETS", true), "background");
	makeNicePlot(assembler->project("ST", true)->plot(true),"S_{T}","GeV")->SaveAs("ttbar_ST_STgt300.png");
	makeNicePlot(assembler->project("HT", true)->plot(true),"H_{T}","GeV")->SaveAs("ttbar_HT_STgt300.png");
	makeNicePlot(assembler->project("MET", true)->plot(true), "E_{T}^{miss}", "GeV")->SaveAs("ttbar_MET_STgt300.png");
	makeNicePlot(assembler->project("NPROMPTNONISOINCLUSIVETRACKS7", true)->plot(false), "nPromptNonIsoTracks")->SaveAs("ttbar_NPROMPTNONISOINCLUSIVETRACKS7_STgt300.png");
	makeNicePlot(assembler->project("NGOODINCLUSIVETRACKS", true)->plot(true), "nInclusiveTracks")->SaveAs("ttbar_NGOODINCLUSIVETRACKS_STgt300.png");
	makeNicePlot(assembler->project("NGOODTRACKS", true)->plot(true),"nTracks")->SaveAs("ttbar_NGOODTRACKS_STgt300.png");
	assembler->project("NGOODTRACKS", true)->print();
	assembler->project("NGOODJETS", true)->print();
	
	delete assembler;
}

